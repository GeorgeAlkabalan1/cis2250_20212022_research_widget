package info.hccis.performancehall_mobileappbasicactivity.dao;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;

@Database(entities = {TicketOrder.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract TicketOrderDAO ticketOrderDAO();
}

