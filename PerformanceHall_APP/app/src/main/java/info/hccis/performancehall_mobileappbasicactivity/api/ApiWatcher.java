package info.hccis.performancehall_mobileappbasicactivity.api;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import info.hccis.performancehall_mobileappbasicactivity.MainActivity;
import info.hccis.performancehall_mobileappbasicactivity.R;
import info.hccis.performancehall_mobileappbasicactivity.ViewOrdersFragment;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrderContent;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrderViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApiWatcher class will be used as a background thread which will monitor the api. It will notify
 * the ui activity if the number of rows changes.
 *
 * @author BJM modified by Mariana Alkabalan 20210402 remodified by BJM 20220202
 * @since 20210329
 */

public class ApiWatcher extends Thread {

    //public static final String API_BASE_URL = "https://bjmac2.hccis.info/api/TicketOrderService/";
    public static final String API_BASE_URL = "http://10.0.2.2:8081/api/TicketOrderService/";

    private int lengthLastCall = -1;  //Number of rows returned

    //The activity is passed in to allow the runOnUIThread to be used.
    private FragmentActivity activity = null;

    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        try {
            do {

                //************************************************************************
                // A lot of this code is duplicated from the TicketOrderContent class.  It will
                // access the api and if if notes that the number of orders has changed, will
                // notify the view order fragment that the data is changed.
                //************************************************************************

                Log.d("BJM api", "running");

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiWatcher.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonTicketOrderApi jsonTicketOrderApi = retrofit.create(JsonTicketOrderApi.class);

                //Create a list of ticket orders.
                Call<List<TicketOrder>> call = jsonTicketOrderApi.getTicketOrders();

                TicketOrderViewModel ticketOrderViewModel = new ViewModelProvider(activity).get(TicketOrderViewModel.class);

                call.enqueue(new Callback<List<TicketOrder>>() {

                    @Override
                    public void onResponse(Call<List<TicketOrder>> call, Response<List<TicketOrder>> response) {

                        //See if we can get the view model.  This contains the list of orders
                        //which is used to populate the recycler view on the list fragment.
                        Log.d("BJM api", "found ticket order view model. size=" + ticketOrderViewModel.getTicketOrders().size());


                        if (!response.isSuccessful()) {
                            Log.d("BJM api", "BJM not successful response from rest for ticket orders Code=" + response.code());

                            //******************************************************************************************
                            // Using the default shared preferences.  Using the application context - may want to access the
                            // shared prefs from other activities.
                            //******************************************************************************************

                            loadFromRoomIfPreferred(ticketOrderViewModel);
                            lengthLastCall = -1; //Indicate couldn't load from api will trigger reload next time

                        } else {
                            //Take the list and pass to constructor of ArrayList to convert it.
                            ArrayList<TicketOrder> ticketOrdersTemp = new ArrayList(response.body()); //note gson will be used implicitly
                            int lengthThisCall = ticketOrdersTemp.size();

                            Log.d("BJM api", "back from api, size=" + lengthThisCall);


                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                                //******************************************************************
                                //Note here.  The recycler view is using the ArrayList from this
                                //ticketOrderViewModel class.  We will take the ticket orders obtained
                                //from the api and add refresh the list .  Will then notify the
                                //recyclerview that things have changed.
                                //******************************************************************
                                ticketOrderViewModel.setTicketOrders(ticketOrdersTemp); //Will addAll
                                Log.d("BJM api ", "First load of ticket orders from the api");

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("BJM api", "trying to notify adapter that things have changed");
                                        ViewOrdersFragment.notifyDataChanged("Found more rows");
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                TicketOrderContent.reloadTicketOrdersInRoom(ticketOrdersTemp);




                            } else if (lengthThisCall != lengthLastCall) {
                                //******************************************************************
                                //data has changed
                                //******************************************************************
                                Log.d("BJM api", "Data has changed");
                                lengthLastCall = lengthThisCall;
                                ticketOrderViewModel.getTicketOrders().clear();
                                ticketOrderViewModel.getTicketOrders().addAll(ticketOrdersTemp);

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("bjm api", "trying to notify adapter that things have changed");
                                        Log.d("bjm api", "Also using method to send a notification to the user");
                                        ViewOrdersFragment.notifyDataChanged("Update - the data has changed", activity, MainActivity.class);
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                TicketOrderContent.reloadTicketOrdersInRoom(ticketOrdersTemp);

                            } else {
                                //*******************************************************************
                                // Same number of rows so don't bother updating the list.
                                //*******************************************************************
                                Log.d("bjm api", "Data has not changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<TicketOrder>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("bjm api", "api call failed");
                        Log.d("bjm api", t.getMessage());

                        //******************************************************************************************
                        // Using the default shared preferences.  Using the application context - may want to access the
                        // shared prefs from other activities.
                        //******************************************************************************************

                        lengthLastCall = -1; //Indicate couldn't load from api will trigger reload next time
                        loadFromRoomIfPreferred(ticketOrderViewModel);

                    }
                });

                //***********************************************************************************
                // Sleep so not checking all the time
                //***********************************************************************************
                final int SLEEP_TIME = 10000;
                Log.d("BJM Sleep", "Sleeping for " + (SLEEP_TIME / 1000) + " seconds");
                Thread.sleep(SLEEP_TIME); //Check api every 10 seconds

            } while (true);
        } catch (InterruptedException e) {
            Log.d("BJM api", "Thread interrupted.  Stopping in the thread.");
        }
    }

    /**
     * Check the shared preferences and load from the db if the setting is set to do such a thing.
     * @param ticketOrderViewModel
     * @since 20220211
     * @author BJM
     */
    public void loadFromRoomIfPreferred(TicketOrderViewModel ticketOrderViewModel) {
        Log.d("BJM room","Check to see if should load from room");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        boolean loadFromRoom = sharedPref.getBoolean(activity.getString(R.string.preference_load_from_room), true);
        Log.d("BJM room","Load from Room="+loadFromRoom);
        if (loadFromRoom) {
            List<TicketOrder> testList = MainActivity.getMyAppDatabase().ticketOrderDAO().selectAllTicketOrders();
            Log.d("BJM room","Obtained ticket orders from the db: "+testList.size());
            ticketOrderViewModel.setTicketOrders(testList); //Will add all ticket orders

            //**********************************************************************
            // This method will allow a call to the runOnUiThread which will be allowed
            // to interact with the ui components of the app.
            //**********************************************************************
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("bjm api", "trying to notify adapter that things have changed");
                    ViewOrdersFragment.notifyDataChanged("Found more rows");
                }

            });

        }
    }


}

