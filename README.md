# CIS2250 Research Component #

### What is this repository for? ###

* This repository will contain widget research component for cis2250
* 2021/2022 AYR

### Who do I talk to? ###

Learning Manager: BJ MacLean

Email: bjmaclean@hollandcollege.com

George Alkabalan

Email: galkabalan@hollandcollege.com


### Contents ###

This repository contains a widget example app, powerpoint presentation, and a video demo